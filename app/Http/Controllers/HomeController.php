<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        //logica

        return view('home.index', [
            "dato" => "Hola clase Laravel"
        ]);
    }

    public function mensaje()
    {
        // logica
        return view('home.mensaje', [
            "mensaje" => "Probando Laravel"
        ]);
    }

    public function listado()
    {
        // $nombres = [
        //     "Juan",
        //     "Pedro",
        //     "Luis",
        //     "Maria"
        // ];

        // saco los nombres de la tabla
        $nombres = DB::table('nombres')->pluck('nombre');

        return view('home.listado', [
            "nombres" => $nombres
        ]);
    }

    public function imagen()
    {
    }
}
